---
title: Does it come with installation and setup documentation?
categories: [presale]
---

Yes, there is an instructional card included with the packaging. Those instructions can be found in the documentation on this site.