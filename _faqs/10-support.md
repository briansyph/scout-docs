---
title: Do you provide customer support?
categories: [presale]
---

Yes. In short, you can go to our [Customer Service](https://scoutsecured.atlassian.net/servicedesk/customer/portal/1) page to submit requests.

#### Support period

Support will always be available to all of our customers.

#### What is included?

Help with defects in a Scout device
Periodic security patching and updates
Technology refreshment on hardware
Feature updates

#### What's not included?

Support does not include alert/notification monitoring, incident response, or security auditing. These are available as separate packages.

#### Contacting support

[Customer Service](https://scoutsecured.atlassian.net/servicedesk/customer/portal/1) page.
