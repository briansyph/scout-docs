---
title: Are updates and bug fixes included in the subscription cost?
categories: [presale]
---

Yes. Bug fixes, feature requests, and general questions can be submitted through our [Customer Service](https://scoutsecured.atlassian.net/servicedesk/customer/portal/1) page.
