---
layout: page
width: expand
hero:
    title: What answers can we scout for you?
    subtitle: Search in depth articles and videos to find every answer you're looking for on the Scout platform
    image: scout-logo.svg
    search: true
---

{% include boxes.html columns="3" title="Browse Topics" subtitle="Choose an option that you need help with or search above" %}

{% include faqs.html multiple="true" title="Frequently asked questions" category="presale" subtitle="Find quick answers to frequently asked questions" %}

<!-- {% include videos.html columns="2" title="Video Tutorials" subtitle="Watch screencasts to get you going with Scout" %} -->

{% include cta.html title="Didn't find an answer?" button_text="Contact Us" button_url="https://scoutsecured.atlassian.net/servicedesk/customer/portal/1" subtitle="Submit a service request to get your issue resolved" %}

