---
title: Organizational Preparation
subtitle: This document covers the setup for administrators preparing their organization
author: brian
tags: [setup]
---

### Decision Points

There are two major decisions the initial administrator needs to make when first connecting to Scout.

1. What other administartors need access to the [webapp](https://app.scoutsecured.com/).
   - These users will be added so they can receive a magic link to log in
2. Will the organization utilize the provided password manager?
   - Yes?
     - Define password policies
     - Identify how to handle infractions
     - Install the [Chrome Extension](https://chromewebstore.google.com/detail/scout-vault/epdnjmipegnbehbahblcaamieihncboo)
   - No?
     - Re-evaluate your current password manager to see if it lives up to industry standard and how much is being paid.

### User Preparation

While connected to the secure network (Bastion), you can expect to see a lot of content blocked. Here are a couple of examples of what may be inaccessible or limited:

- Google Sponsored Links
- Streaming services that included ads (Hulu, Disney+, etc.)
- Social Media "Recommended For You" content
- Explicit content (porn, gambling, shock sites, etc.)
- Malicious content

Beyond the restricted network, it's important for users to understand that their devices are being scanned and monitored while connected to the network.

All users should be aware of how to submit requests if they think Scout is blocking something business-critical.