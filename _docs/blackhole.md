---
title: Blackhole
subtitle: This document details Scout's DNS server, Blackhole
author: brian
tags: [features]
---

## Summary

The Scout DNS server is named Blackhole. The primary purpose of Blackhole is to block malicious, suspicious, and inappropriate traffic. To accomplish that, Blackhole leverages a multitude of community maintained lists which includes domains for:

    - Advertisements and trackers
    - Known spam
    - Ransomware C2
    - Malware C2
    - Phishing
    - Crypto Scams and Miners
    - Shock (Deceiving domains hosting shockingly explicit images)
    - Pornography
    - Gambling

## Configuration

Blackhole is preconfigured and does not require any action from the user. However, changes can be requested via a ticket with [customer service](https://support.scoutsecured.com/servicedesk/customer/portal/1).

Common requests include extra blocking topics and unblocking business-critical connections.