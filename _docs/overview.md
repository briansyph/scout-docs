---
title: Feature Overview
subtitle: This document covers the overview of Scout's features
author: brian
tags: [features]
---

## Overview

At Scout Secured, we offer a full suite of cybersecurity tools and practices tailored to companies of all sizes. Our mission is to empower businesses with robust security measures, ensuring a safe operating environment for teams.

Through Scout Secured, companies gain access to essential services aimed at fortifying their networks, safeguarding team members, and protecting sensitive company data. Our primary focus is on providing users with convenient and cost-effective solutions to bolster their security posture.

With our Scout devices (nodes), your team can effortlessly connect to your organization's network from anywhere in the world with confidence. These nodes utilize existing internet networks to establish a secure and private virtual HQ for your organization, ensuring complete privacy and anonymity.

## Provided Services

### Encrypted site-to-site Virtual Private Network (VPN)

Scout Secured's VPN is the cornerstone of our solution, it seamlessly links multiple remote networks into a unified virtual environment. With Scout Secured’s VPN, your organization can consolidate disparate networks, fostering collaboration and productivity across geographically dispersed teams.

Our VPN solution operates effortlessly, deploying your Scout network over any existing internet connection. This flexibility ensures that regardless of location or network infrastructure, your team can securely access company resources with ease. Whether employees are working from home, traveling, or stationed in remote locations, Scout Secured’s VPN provides a reliable and encrypted connection for uninterrupted workflow.

### Content Filtering (Blackhole)

Blackhole provides content filtering and blocks ads and trackers network-wide for any device connected to it. By default it blocks known ads, trackers, malicious sites, shock sites, pornography, and gambling sites.

### Network Visibility and Security (Sentry)

Sentry is an aggregation of security tools and techniques to provide a greater vision and security of the network. It will show both an internal and external view of the network to help an administrator fix any identified vulnerabilities.

### Password Manager (Vault)

Scout provides a vault powered by BitWarden to secure passwords. An app for both Android and iPhone is available in the respective app stores.
