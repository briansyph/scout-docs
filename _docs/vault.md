---
title: Vault
subtitle: This document details Scout's password manage, Vault
author: brian
tags: [features]
---

## Summary

Vault is an industry standard password manager.

## Configuration

Install the [Chrome Extension](https://chromewebstore.google.com/detail/scout-vault/epdnjmipegnbehbahblcaamieihncboo).

To create an account ensure the device is connected to the VPN:

1. Confirm `Logging in on:` is set to `Scout`. Then, click `Create account` in the plugin

   ![vault-account-creation](/uploads/vault-account-creation.png "Create")

2. Input required information and ensure to check the box accepting the ToS and Privacy Policy

   ![vault-account-details](/uploads/vault-account-details.png "Details")

3. Follow any emailed instructions

## Administrators

To perform administrator actions, ensure the primary operating device is connected to the VPN.

1. Log into the [Vault admin console](https://app.scoutsecured.com/vault/admin) with the provided credentials
2. Set proper organizational expectations