---
title: Sentry
subtitle: This document details Scout's security suite, Sentry
author: brian
tags: [features]
---

## Summary

Sentry goes beyond a industry standard security stack by securing and monitoring the internal VPN, external assets, and human risk for an organization.

Internal VPN:  
 - Intrusion Detection System (powered by dynamic threat intelligence)  
 - Inrusion Protection System  
 - Vulnerability Scanning

External Assets:  
 - Vulnerability Scanning  
 - Web Application Auditing

Human Risk:  
 - Dark web monitoring organization wide  
 - Dossier creation via regular web correlations  

## Scout Score

The Scout Score is Scout's way of creating a numeric grade for organizational risk. It is based on a perfect score of `100` and decreases as more risk is identified in the organization. Each aspect of Sentry that generates findings will feed into the Scout Score. Every finding has its own weight and the source of the finding could increase that weight if it's an important system.

## Configuration

Sentry is preconfigured and does not require any action from the user. However, changes can be requested via a ticket with [customer service](https://support.scoutsecured.com/servicedesk/customer/portal/1).

Common requests include modifications to what is blocked by the IPS.