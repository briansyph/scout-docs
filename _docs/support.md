---
title: Contacting support
subtitle: Let us know how we can best serve you
author: brian
tags: [help]

---

Customer support is provided through our [customer service](https://support.scoutsecured.com/servicedesk/customer/portal/1) page. Requests submitted via the portal will become our highest priority.
