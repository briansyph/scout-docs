---
title: Initial Node Setup
subtitle: This document covers the setup for users with a node
author: brian
tags: [setup]
---

### Instructions

Scout devices are plug-n-play so the initial setup is simple.

1. Plug provided ethernet cable to the WAN port on your Scout unit.
2. Plug the other end of your cable to any LAN port on your internet router.
3. Plug in the power cable to your Scout unit and allow up to 5 minutes for setup.
4. The light on the front of your Scout unit should have changed from pulsing blue to solid white.
5. Connect to your secure Wifi network
    - Network Name: `Bastion`
    - Password:     `ScoutSecured`
{% include alert.html style="danger" text="Submit a ticket with support if you experience difficulties." %}


### Notes

1. Nodes can be brought on the road to be used wired or wirelessly anywhere a network is available.
2. Devices connected to `Bastion` are being monitored and actively scanned.