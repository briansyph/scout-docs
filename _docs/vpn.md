---
title: VPN
subtitle: This document details Scout's VPN
author: brian
tags: [features]
---

## Summary

Accessing the VPN is primarily done through the `Bastion` wifi network provided by the Scout nodes. However, it can also be accessed directly on a mobile device (laptop, phone, tablet, etc.). It's more secure to connect via `Bastion` as it creates a secure enclave protecting the connected devices from the rest of the network. While on the VPN, the device will be scanned but still visible to other devices.

### Direct Connection Configurations

1. Log into the [app](https://app.scoutsecured.com/)
   - Only administrators will be able to log in
2. Click on the user menu in the top right and click `Settings`

   ![user-menu-dropdown](/uploads/user-menu-dropdown.png "Menu")

3. Select an option below `VPN Licenses`

   ![vpn-licesnses](/uploads/vpn-licenses.png "Licenses")

4. Scan the QR code in your VPN app on a mobile device or download the configuration file by clicking `VPN FILE`.
